// Bai 1: Tìm số nguyên dương nhỏ nhất

function tinhSoNhoNhat() {
  var sum = 0;
  for (var i = 0; sum < 10000; i++) {
    sum += i;
    if (sum >= 10000) {
      break;
    }
  }
  var n = i;
  document.getElementById("result-ex-1").innerHTML = ` Kết quả là: ${n}`;
}

// Bài 2: Tính tổng

function tinhTong() {
  var x = document.getElementById("txtX").value * 1;
  var n = document.getElementById("txtN").value * 1;

  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  document.getElementById("result-ex-2").innerHTML = `Kết quả là: ${sum}`;
}

// Bài 3: Tính giai thừa

function tinhGiaiThua() {
  var a = document.getElementById("txtA").value * 1;

  var giaiThua = 1;

  if (a > 0) {
    for (var i = 1; i <= a; i++) {
      giaiThua = giaiThua * i;
    }
  } else if (a == 0) {
    giaiThua = 1;
  } else {
    alert("a phải là số nguyên dương");
  }

  document.getElementById("result-ex-3").innerHTML = ` Kết quả là: ${giaiThua}`;
}

// Bài 4: Tạo thẻ DIV

function taoTheDIV() {
  var content = "";

  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content = content + "<div style='background-color: red' >div chẵn</div>";
    } else {
      content = content + "<div style='background-color: blue'>div lẻ</div>";
    }
  }

  document.getElementById("result-ex-4").innerHTML = `${content}`;
}
